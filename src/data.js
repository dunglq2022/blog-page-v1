import blog_image1 from "./assets/img/1.jpg";
import blog_image2 from "./assets/img/2.jpg";
import blog_image3 from "./assets/img/3.jpg";

const items = [
    {
        image: blog_image1,
        time: "Dec 22, 2023",
        h2: "Meet AutoManage, the best AI management tools",
        p: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    },
    {
        image: blog_image2,
        time: "Mar 15, 2023",
        h2: "How to earn more money as a wellness coach",
        p: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    },
    {
        image: blog_image3,
        time: "Jan 05, 2023",
        h2: "The no-fuss guide to upselling and cross selling",
        p: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    },
];

export default items;
