import items from '../data.js'
import { BlogItem } from './BlogItem'

export const Blog = () => {
    return (
        <div className="mt-[120px] flex flex-col items-center">
            <p className="text-[#3056D3] text-[18px] font-[600]">Our Blogs</p>
            <h1 className="text-[#2E2E2E] text-[40px] font-[700] mt-2">Our Recent News</h1>
            <div className="w-[770px] text-center">
                <p className="text-[#637381] text-[15px] font-[400] mt-[15px]">There are many variations of passages of Lorem Ipsum available
but the majority have suffered alteration in some form.</p>          
            </div>
            <div className='flex gap-[30px] mt-[80px]'>{
                    items.map((item) => {
                        return(
                            <BlogItem data={item}/>
                        )
                    })
                }
            </div>     
        </div>
    )
}