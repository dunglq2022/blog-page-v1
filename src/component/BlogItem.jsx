export const BlogItem = ({data}) => {
    return (
        <article className="w-[370px] h-[438px]">
            <img src={data.image} alt="" />
            <p className="mt-[30px] bg-[#3056D3] text-[#fff] w-[120px] pt-1 pb-1 ps-5 pe-5 text-[12px] font-[600] rounded-[5px] leading-6" >{data.time}</p>
            <h2 className="mt-[25px] text-[#212B36] text-[24px] font-[600] leading-6">{data.h2}</h2>
            <p className="mt-[15px] text-[#637381] leading-[28px] font-[400] text-[16px]" >{data.p}</p>
        </article>
    )
}